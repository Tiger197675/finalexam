using TutorPsu.Models;
 using Microsoft.EntityFrameworkCore;
 using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

 namespace TutorPsu.Data 
{ public class TutorPsuContext : IdentityDbContext<TutorUser>
{
    public DbSet<Subject> SubjectsList { get; set; }
    public DbSet<SubjectCategory> SubjectCategory { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
    { 
        optionsBuilder.UseSqlite(@"Data source=TutorPsu.db"); 
        }
    }
}