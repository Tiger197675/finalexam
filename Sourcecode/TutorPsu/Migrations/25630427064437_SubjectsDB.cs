﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TutorPsu.Migrations
{
    public partial class SubjectsDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SubjectCategory",
                columns: table => new
                {
                    SubjectCategoryID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShortName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectCategory", x => x.SubjectCategoryID);
                });

            migrationBuilder.CreateTable(
                name: "SubjectsList",
                columns: table => new
                {
                    SubjectID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SubjectCategoryID = table.Column<int>(nullable: false),
                    ReportDate = table.Column<string>(nullable: true),
                    SubjectDetail = table.Column<string>(nullable: true),
                    SubjectCost = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectsList", x => x.SubjectID);
                    table.ForeignKey(
                        name: "FK_SubjectsList_SubjectCategory_SubjectCategoryID",
                        column: x => x.SubjectCategoryID,
                        principalTable: "SubjectCategory",
                        principalColumn: "SubjectCategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubjectsList_SubjectCategoryID",
                table: "SubjectsList",
                column: "SubjectCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubjectsList");

            migrationBuilder.DropTable(
                name: "SubjectCategory");
        }
    }
}
