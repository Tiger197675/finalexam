using System; 
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace TutorPsu.Models 
{ 
  public class TutorUser : IdentityUser{ 
    public string FirstName { get; set;} 
    public string LastName { get; set;} 
    
}
  public class SubjectCategory { 
      public int SubjectCategoryID { get; set; } 
      public string ShortName {get; set; } 
      public string FullName { get; set; }  
}

       public class Subject { 
        public int SubjectID { get; set; } 
        public int SubjectCategoryID { get; set; } 
        public SubjectCategory SubejectCat { get; set; }
        
        [DataType(DataType.Date)] 
        public string ReportDate { get; set; } 
        public string SubjectDetail { get; set; } 
    
        public string SubjectCost { get; set; }

        public string UserId {get; set;} 
        public TutorUser postUser {get; set;} 
        } 
      }

    