﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages
{
    public class IndexModel : PageModel
    {
     private readonly TutorPsu.Data.TutorPsuContext _context;

        public IndexModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        public IList<Subject> Subject { get;set; }
        public IList<SubjectCategory> SubjectCategory { get; set;}

        public async Task OnGetAsync()
        {
            Subject = await _context.SubjectsList
                .Include(s => s.SubejectCat).ToListAsync();

            SubjectCategory = await _context.SubjectCategory.ToListAsync();        }
    }
}
