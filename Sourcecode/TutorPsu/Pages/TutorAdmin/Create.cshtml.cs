using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages.TutorAdmin
{
    public class CreateModel : PageModel
    {
        private readonly TutorPsu.Data.TutorPsuContext _context;

        public CreateModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["SubjectCategoryID"] = new SelectList(_context.SubjectCategory, "SubjectCategoryID", "ShortName");
            return Page();
        }

        [BindProperty]
        public Subject Subject { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.SubjectsList.Add(Subject);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}