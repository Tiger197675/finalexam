using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages.TutorAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly TutorPsu.Data.TutorPsuContext _context;

        public DeleteModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Subject Subject { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Subject = await _context.SubjectsList
                .Include(s => s.SubejectCat).FirstOrDefaultAsync(m => m.SubjectID == id);

            if (Subject == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Subject = await _context.SubjectsList.FindAsync(id);

            if (Subject != null)
            {
                _context.SubjectsList.Remove(Subject);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
