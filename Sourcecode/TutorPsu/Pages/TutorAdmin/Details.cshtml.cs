using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages.TutorAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly TutorPsu.Data.TutorPsuContext _context;

        public DetailsModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        public Subject Subject { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Subject = await _context.SubjectsList
                .Include(s => s.SubejectCat).FirstOrDefaultAsync(m => m.SubjectID == id);

            if (Subject == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
