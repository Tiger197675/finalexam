using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages.TutorAdmin
{
    public class EditModel : PageModel
    {
        private readonly TutorPsu.Data.TutorPsuContext _context;

        public EditModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Subject Subject { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Subject = await _context.SubjectsList
                .Include(s => s.SubejectCat).FirstOrDefaultAsync(m => m.SubjectID == id);

            if (Subject == null)
            {
                return NotFound();
            }
           ViewData["SubjectCategoryID"] = new SelectList(_context.SubjectCategory, "SubjectCategoryID", "SubjectCategoryID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Subject).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubjectExists(Subject.SubjectID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool SubjectExists(int id)
        {
            return _context.SubjectsList.Any(e => e.SubjectID == id);
        }
    }
}
