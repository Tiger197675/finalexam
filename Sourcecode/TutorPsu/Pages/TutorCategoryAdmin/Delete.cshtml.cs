using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages.TutorCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly TutorPsu.Data.TutorPsuContext _context;

        public DeleteModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        [BindProperty]
        public SubjectCategory SubjectCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SubjectCategory = await _context.SubjectCategory.FirstOrDefaultAsync(m => m.SubjectCategoryID == id);

            if (SubjectCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SubjectCategory = await _context.SubjectCategory.FindAsync(id);

            if (SubjectCategory != null)
            {
                _context.SubjectCategory.Remove(SubjectCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
