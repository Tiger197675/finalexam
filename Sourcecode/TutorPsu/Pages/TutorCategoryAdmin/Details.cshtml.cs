using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages.TutorCategoryAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly TutorPsu.Data.TutorPsuContext _context;

        public DetailsModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        public SubjectCategory SubjectCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SubjectCategory = await _context.SubjectCategory.FirstOrDefaultAsync(m => m.SubjectCategoryID == id);

            if (SubjectCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
