using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TutorPsu.Data;
using TutorPsu.Models;

namespace TutorPsu.Pages.TutorCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly TutorPsu.Data.TutorPsuContext _context;

        public EditModel(TutorPsu.Data.TutorPsuContext context)
        {
            _context = context;
        }

        [BindProperty]
        public SubjectCategory SubjectCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SubjectCategory = await _context.SubjectCategory.FirstOrDefaultAsync(m => m.SubjectCategoryID == id);

            if (SubjectCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(SubjectCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubjectCategoryExists(SubjectCategory.SubjectCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool SubjectCategoryExists(int id)
        {
            return _context.SubjectCategory.Any(e => e.SubjectCategoryID == id);
        }
    }
}
